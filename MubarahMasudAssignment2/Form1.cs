﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MubarahMasudAssignment2
{
    public partial class Form1 : Form
    {
        SqlConnection con;
        SqlCommand cmd;
        BindingSource bs;

        public Form1()
        {
            InitializeComponent();
            con =
                new SqlConnection(
                    @"Data Source=(localdb)\ProjectsV13;Initial Catalog=MovieDatabase;Integrated Security=True;Connect Timeout=30;Encrypt=False;TrustServerCertificate=False;ApplicationIntent=ReadWrite;MultiSubnetFailover=False");
        }

        //SELECT Query to display table when Program runs
        private void Form1_Load(object sender, EventArgs e)
        {
            try
            {
                con.Open();
                using (cmd = new SqlCommand("Select * from Movie", con))
                {
                    using (SqlDataReader rdr = cmd.ExecuteReader())
                    {
                        bs = new BindingSource();
                        bs.DataSource = rdr;
                        dataGridView1.DataSource = bs;

                        con.Close();
                    }
                }
            } catch (Exception ex)
            {
                MessageBox.Show("There is a problem in the Select Query result.");
            }
        }

        //ADD BUTTON
        private void btnAdd_Click(object sender, EventArgs e)
        {
            try
            {
                using (cmd = new SqlCommand("Insert into Movie values (@Id, @Name, @ISBN, @Date, @Location, @Genre, @Rating, @Duration, @Price)", con))
                {
                    cmd.Parameters.AddWithValue("@Id", Convert.ToInt32(txtID.Text));
                    cmd.Parameters.AddWithValue("@Name", txtName.Text);
                    cmd.Parameters.AddWithValue("@ISBN", Convert.ToInt32(txtISBN.Text));
                    cmd.Parameters.AddWithValue("@Date", dtpDate.Value.Date);
                    cmd.Parameters.AddWithValue("@Location", cbLocation.SelectedItem);
                    cmd.Parameters.AddWithValue("@Genre", cbGenre.SelectedItem);
                    cmd.Parameters.AddWithValue("@Rating", cbRating.SelectedItem);
                    cmd.Parameters.AddWithValue("@Duration", Convert.ToInt32(txtDuration.Text));
                    cmd.Parameters.AddWithValue("@Price", Convert.ToDouble(txtPrice.Text));

                    con.Open();
                    int x = cmd.ExecuteNonQuery();
                    con.Close();
                    if (x == 1)
                        MessageBox.Show("Record Inserted");
                    else
                        MessageBox.Show("Something went wrong.");
                    con.Open();
                    cmd = new SqlCommand("Select * from Movie", con);
                    using (SqlDataReader rdr = cmd.ExecuteReader())
                    {
                        bs.DataSource = rdr;
                        con.Close();
                        dataGridView1.DataSource = bs;
                    }
                }
            }
            catch (Exception ex)
            {   
                //Double check to see if user has passed validation and has clicked Add Button
                if (txtID.Text == null || txtName.Text == null || txtISBN.Text == null || dtpDate.Value.Date < DateTime.Now.Date
                    || cbLocation.SelectedItem == null || cbGenre.SelectedItem == null || cbRating.SelectedItem == null || txtDuration.Text == null || txtPrice.Text == null)
                {
                    MessageBox.Show("Please fill in all fields before adding to Database");
                }
            }
        }

        //UPDATE BUTTON
        private void btnUpdate_Click(object sender, EventArgs e)
        {
            try
            {             
              using (cmd = new SqlCommand("Update Movie Set Name = '" + txtName.Text +
                                                                     "',ISBN = '" + txtISBN.Text +
                                                                     "',Date = '" + dtpDate.Value.Date +
                                                                     "',Location = '" + cbLocation.SelectedItem +
                                                                     "',Genre = '" + cbGenre.SelectedItem +
                                                                     "',Rating = '" + cbRating.SelectedItem +
                                                                     "',Duration = '" + txtDuration.Text +
                                                                     "',Price = '" + txtPrice.Text + "' where Id =" + txtID.Text, con))
                {
                    con.Open();
                    int x = cmd.ExecuteNonQuery();
                    con.Close();
                    if (x == 1)
                        MessageBox.Show("Record Updated.");
                    else
                        MessageBox.Show("Please add Existing Movie ID to Update Record.");
                    con.Open();
                    cmd = new SqlCommand("Select * from Movie", con);
                    using (SqlDataReader rdr = cmd.ExecuteReader())
                    {
                        bs.DataSource = rdr;
                        con.Close();
                        dataGridView1.DataSource = bs;
                    }
                }
            }
            catch (Exception ex)
            {
                //If user clicks Update without specifying Movie ID
                MessageBox.Show("There was a problem Updating the Record in the table.");
            }
        }

        //DELETE BUTTON
        private void btnDelete_Click(object sender, EventArgs e)
        {
            try
            {
                using (cmd = new SqlCommand("Delete from Movie where Id=" + txtID.Text, con))
                {
                    con.Open();
                    int x = cmd.ExecuteNonQuery();
                    con.Close();
                    if (x == 1)
                        MessageBox.Show("Record Deleted.");
                    else
                        MessageBox.Show("Please add Existing Movie ID to Delete Record.");
                    con.Open();
                    cmd = new SqlCommand("Select * from Movie", con);
                    using (SqlDataReader rdr = cmd.ExecuteReader())
                    {
                        bs.DataSource = rdr;
                        con.Close();
                        dataGridView1.DataSource = bs;
                    }
                }

            }
            catch (Exception ex)
            {
                //If user clicks Delete without specifying Movie ID
                MessageBox.Show("There was a problem Deleting the Record in the table");
            }
        }

        //CLEAR BUTTON to clear all control data
        private void btnClear_Click(object sender, EventArgs e)
        {
            txtID.Text = "";
            txtName.Text = "";
            txtISBN.Text = "";
            dtpDate.ResetText();
            cbLocation.SelectedItem = null;
            cbGenre.SelectedItem = null;
            cbRating.SelectedItem = null;
            txtDuration.Text = "";
            txtPrice.Text = "";
        }

        //MOVIE ID VALIDATION
        private bool ValidMovieID(string movieID, out string errorMessage)
        {
            if (movieID.Length == 0)
            {
                errorMessage = "Movie ID is required";
                return false;
            }
            if ((Regex.IsMatch(movieID, @"^[0-9]+$")) && movieID.Length == 3)
            {
                errorMessage = "";
                return true;
            }
            else
            {
                errorMessage = "Movie ID is a 3-digit number";
                return false;
            }
        }

        private void txtID_Validating(object sender, CancelEventArgs e)
        {
            string errorMsg;

            if (!ValidMovieID(txtID.Text, out errorMsg))
            {
                e.Cancel = true;
                txtID.Select(0, txtID.Text.Length);
                ep_ID.SetError(txtID, errorMsg);
            }
        }

        private void txtID_Validated(object sender, EventArgs e)
        {
            ep_ID.SetError(txtID, "");
        }

        //MOVIE NAME VALIDATION
        public bool ValidMovieName(string movieName, out string errorMessage)
        {
            if (movieName.Length == 0)
            {
                errorMessage = "Movie Name is required";
                return false;
            }
            errorMessage = "";
            return true;
        }

        private void txtName_Validating(object sender, CancelEventArgs e)
        {
            string errorMsg;

            if (!ValidMovieName(txtName.Text, out errorMsg))
            {
                e.Cancel = true;
                txtName.Select(0, txtName.Text.Length);
                this.ep_Name.SetError(txtName, errorMsg);
            }
        }

        private void txtName_Validated(object sender, EventArgs e)
        {
            ep_Name.SetError(txtName, "");
        }


        //ISBN NUMBER VALIDATION
        private bool ValidISBNNumber(string isbnNumber, out string errorMessage)
        {
            if (isbnNumber.Length == 0)
            {
                errorMessage = "ISBN Number is required";
                return false;
            }
            if ((Regex.IsMatch(isbnNumber, @"^[0-9]+$")) && isbnNumber.Length == 5)
            {
                errorMessage = "";
                return true;
            }
            else
            {
                errorMessage = "ISBN can only be a 5-digit number";
                return false;
            }
        }

        private void txtISBN_Validating(object sender, CancelEventArgs e)
        {
            string errorMsg;

            if (!ValidISBNNumber(txtISBN.Text, out errorMsg))
            {
                e.Cancel = true;
                txtISBN.Select(0, txtISBN.Text.Length);
                ep_ISBN.SetError(txtISBN, errorMsg);
            }
        }
        private void txtISBN_Validated(object sender, EventArgs e)
        {
            ep_ISBN.SetError(txtISBN, "");
        }

        //DATE VALIDATION
        private bool ValidDate(string date, out string errorMessage)
        {
            if (dtpDate.Value.Date < DateTime.Now.Date)
            {
                errorMessage = "Please select Today or a Date in the future";
                return false;
            }
            errorMessage = "";
            return true;
        }

        private void dtpDate_Validating(object sender, CancelEventArgs e)
        {
            string errorMsg;

            if (!ValidDate(dtpDate.Value.Date.ToString(), out errorMsg))
            {
                e.Cancel = true;
                ep_Date.SetError(dtpDate, errorMsg);
            }
        }

        private void dtpDate_Validated(object sender, EventArgs e)
        {
            ep_Date.SetError(dtpDate, "");

        }

        private void cbLocation_Validating(object sender, CancelEventArgs e)
        {
            bool cancel = false;
            if (cbLocation.SelectedItem == null)
            {
                cancel = true;
                this.ep_Location.SetError(this.cbLocation, "Please select a location");
            }

            e.Cancel = cancel;
        }

        private void cbLocation_Validated(object sender, EventArgs e)
        {
            ep_Location.SetError(cbLocation, "");
        }

        //GENRE VALIDATION
        private void cbGenre_Validating(object sender, CancelEventArgs e)
        {
            bool cancel = false;
            if (cbGenre.SelectedItem == null)
            {
                cancel = true;
                this.ep_Genre.SetError(this.cbGenre, "Please select a Movie Genre");
            }

            e.Cancel = cancel;
        }

        private void cbGenre_Validated(object sender, EventArgs e)
        {
            ep_Genre.SetError(cbGenre, "");
        }

        //RATING VALIDATION
        private void cbRating_Validating(object sender, CancelEventArgs e)
        {
            bool cancel = false;
            if (cbRating.SelectedItem == null)
            {
                cancel = true;
                this.ep_Rating.SetError(this.cbRating, "Please select a Movie Rating");
            }

            e.Cancel = cancel;
        }

        private void cbRating_Validated(object sender, EventArgs e)
        {
            ep_Rating.SetError(cbRating, "");
        }


        //DURATION VALIDATION 
        private bool ValidDuration(string duration, out string errorMessage)
        {
            if (duration.Length == 0)
            {
                errorMessage = "ISBN Number is required";
                return false;
            }

            if ((Regex.IsMatch(duration, @"^[0-9]+$")) && duration.Length <= 4)
            {
                errorMessage = "";
                return true;
            }
            else
            {
                errorMessage = "Duration can only be in minutes.";
                return false;
            }
        }

        private void txtDuration_Validating(object sender, CancelEventArgs e)
        {
            string errorMsg;

            if (!ValidDuration(txtDuration.Text, out errorMsg))
            {
                e.Cancel = true;
                txtDuration.Select(0, txtDuration.Text.Length);
                ep_Duration.SetError(txtDuration, errorMsg);
            }
        }

        private void txtDuration_Validated(object sender, EventArgs e)
        {
            ep_Duration.SetError(txtDuration, "");
        }


        //PRICE VALIDATION
        private bool ValidPrice(string price, out string errorMessage)
        {
            if (price.Length == 0)
            {
                errorMessage = "ISBN Number is required";
                return false;
            }

            if ((Regex.IsMatch(price, @"([0-9]+\.[0-9]*)|([0-9]*\.[0-9]+)|([0-9]+)")))
            {
                errorMessage = "";
                return true;
            }
            else
            {
                errorMessage = "Please enter a decimal value for price.";
                return false;
            }
        }

        private void txtPrice_Validating(object sender, CancelEventArgs e)
        {
            string errorMsg;

            if (!ValidPrice(txtPrice.Text, out errorMsg))
            {
                e.Cancel = true;
                txtPrice.Select(0, txtPrice.Text.Length);
                ep_Price.SetError(txtPrice, errorMsg);
            }
        }

        private void txtPrice_Validated(object sender, EventArgs e)
        {
            ep_Price.SetError(txtPrice, "");
        }


    }//closing braces
}
